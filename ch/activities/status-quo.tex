\section{Analysis of the Status Quo}
\label{sec:status-quo}

In order to get a clear image of the work that still needs to be done, the 
current state of the software is analyzed. This is done by mapping the 
architecture, i.e. the components that make up the software and how they are 
related. Furthermore, the functionality of the software is tested using unit 
tests. Using these tests it can be determined whether the software meets the 
requirements that have been defined in \autoref{ch:background}.

    \subsection{Client--Server Communication}
    \label{subsec:architecture}
    
    As order queries are particularly useful to client--server communication, 
    the initial code base provides ways to simulate such a use-case. To be 
    able to use this software, it is important to understand how it is 
    implemented, which component does what and how these components are 
    related. The code for the client and server --- \texttt{client.py} and 
    \texttt{server.py} respectively --- can be run in order to simulate 
    the communication between a server and client in a real-life use-case. 
    Both components utilize treap nodes to store data and communicate via 
    \emph{XML-RPC}. The process of inserting node into the server's database has been visualized in \autoref{fig:insert-nodes}. Different configurations can be used to store date either 
    as plain text, \gls{ore} or \gls{gc}. The client can be provided with a 
    number that represents that amount of nodes to be inserted into the treap.
    Each node contains either plaintext or data encrypted with either 
    \gls{ore} or \gls{gc}, encapsulated in a wrapper class.

   % Using \emph{PyCharm}'s debugging functionality, the running of the 
   % server can be inspected during communication with the server. As a result,
   % the amount of \emph{round-trips} can be determined, e.g. when using a 
   % \gls{ore} treap with three nodes, the following functions of the server 
   % are called, in order:

   % \begin{enumerate}
   %     \item \texttt{server.insert()}
   %     \item \texttt{server.insert()}
   %     \item \texttt{server.insert()}
   %     \item \texttt{server.count()}
   %     \item \texttt{server.query()}
   %     \item \texttt{server.query()}
   %     \item \texttt{server.query()}
   %     \item \texttt{server.smallest()}
   %     \item \texttt{server.biggest()}
   % \end{enumerate}

   % Looking at this code, it is evident that the server does not use round-
   % trips to insert or query the nodes. This means that the treap is traversed 
   % completely on the server side. Using the the debugger, we can also determine the 
   % sequence of functions that are evaluated when the client inserts nodes 
   % into the server. This sequence has been visualized in \autoref{fig:insert-nodes}.

    \begin{figure}[h]
        \centering
        \begin{tikzpicture}
            \tikzstyle{block} = [
                draw,
                minimum width = 3.5cm
            ];

            \tikzstyle{member} = [
                draw,
                minimum width = 2.5cm,
                minimum height = 1cm,
                font = \ttfamily\small
            ];

            \node (client) at (-4, 0) [
                block,
                minimum height = 2cm
            ] {};
            \node (insert-nodes) at (-4, 0) [
                member
            ] {insert\_nodes};

            \node (gc) at (-4, -3.25) [
                block,
                minimum height = 3.5cm
            ] {};
            \node (generate) at (-4, -2.5) [
                member
            ] {generate};
            \node (evaluate) at (-4, -4) [
                member
            ] {evaluate};

            \node (node) at (0, -3.25) [
                block,
                minimum height = 3.5cm
            ] {};
            \node (init) at (0, -2.5) [
                member
            ] {\_\_init\_\_};
            \node (compare) at (0, -4) [
                member
            ] {compare};
            
            \node (server) at (4, 0) [
                block,
                minimum height = 2cm
            ] {};
            \node (insert) at (4, 0) [
                member
            ] {insert};

            \node (treap) at (4, -2.5) [
                block,
                minimum height = 2cm
            ] {};
            \node (setitem) at (4, -2.5) [
                member
            ] {\_\_setitem\_\_};

            \node (treap-node) at (4, -6.25) [
                block,
                minimum height = 3.5cm
            ] {};
            \node (ninsert) at (4, -5.5) [
                member
            ] {insert};
            \node (pyx-insert) at (4, -7) [
                member
            ] {pyx\_insert};

            %\draw (insert-nodes) -- (generate) -- (init) -- (insert) -- (setitem) -- (insert) -- (pyx-insert) -- (compare) -- (evaluate);

            \draw [->] (insert-nodes) -- (generate);
            \draw [->] (generate) -- (init);
            \draw [->] (init) -- (insert);
            \draw [->] (insert) -- (setitem);
            \draw [->] (setitem) -- (ninsert);
            \draw [->] (ninsert) -- (pyx-insert);
            \draw [->] (pyx-insert) -- (compare);
            \draw [->] (compare) -- (evaluate);

            \node [
                above = 0.25cm of client
            ] {Client};
            \node [
                below = 0.25cm of gc
            ] {GC};
            \node [
                above = 0.25cm of node
            ] {Node};
            \node [
                above = 0.25cm of server
            ] {Server};
            \node [
                below = 0.25cm of treap
            ] {Treap};
            \node [
                below = 0.25cm of treap-node
            ] {Treap Node};

        \end{tikzpicture}

        \caption{Illustration of the function sequence trigged when 
                 the client inserts nodes into the database, starting 
                 at \texttt{insert\_nodes} and ending at the 
                 \texttt{evaluate} method. This visualization assumes that GC is used.}
        \label{fig:insert-nodes}
    \end{figure}

    %\begin{figure}[h]
    %    \centering
    %    \begin{tikzpicture}
    %        \node (node) at (0, 0) [
    %            draw,
    %            minimum height = 2cm,
    %            minimum width = 6cm,
    %            rounded corners = 1cm
    %        ] {};

    %        \node (node-label) [
    %            right = 0.25cm of node
    %        ] {Node};

    %        \node (server) [
    %            draw,
    %            above left = 2cm and 0.5cm of node,
    %            ellipse
    %        ] {Server};

    %        \node (client) [
    %            draw,
    %            above right = 2cm and 0.5cm of node,
    %            ellipse
    %        ] {Client};

    %        \draw [
    %            <->,
    %            bend right = 45
    %        ] (server) to (client);

    %        \node (plain) at (-2, 0) [
    %            draw,
    %            circle,
    %            minimum width = 1.5cm
    %        ] {\small\texttt{Plain}};

    %        \node (ore) at (0, 0) [
    %            draw,
    %            circle,
    %           % minimum width = 1.5cm
    %        ] {\small\texttt{ORE}};

    %        \node (gc) at (2, 0) [
    %            draw,
    %            circle,
    %            minimum width = 1.5cm
    %        ] {\small\texttt{GC}};

    %        \node (lewi) at (0, -2) {\small\texttt{LewiWuOREBlkLF}};

    %        \node (arx) at (2, -2) {\small\texttt{ARXGC}};

    %        \draw [->] (ore) -- (lewi);
    %        \draw [->] (gc) -- (arx);

    %    \end{tikzpicture}
    %    \caption{A graphical illustration of the communication between 
    %             the client and the server. The actual data being communicated 
    %             is encapsulated in a node and a wrapper for either plaintext, 
    %             \gls{ore} or \gls{gc}.}
    %    \label{fig:client-server}
    %\end{figure}

   % \begin{figure}
   %     \centering
   %     \begin{tikzpicture}
   %         \node (server) at (0, -0.25) [
   %             draw,
   %             minimum width = 5cm,
   %             minimum height = 3.5cm
   %         ] {};

   %         \node (client) at (0, 3) [
   %             draw,
   %             minimum width = 5cm,
   %             minimum height = 2cm
   %         ] {};

   %         \node (node-ca) at (-1.5, 3) [
   %             draw,
   %             circle,
   %             minimum width = 1cm
   %         ] {a};
   %         \node (node-cb) at (0, 3) [
   %             draw,
   %             circle,
   %             minimum width = 1cm
   %         ] {b};
   %         \node (node-cc) at (1.5, 3) [
   %             draw,
   %             circle,
   %             minimum width = 1cm
   %         ] {c};

   %         \node (node-sa) at (-1.5, -1) [
   %             draw,
   %             circle,
   %             minimum width = 1cm
   %         ] {a};

   %         \node (node-sb) at (0, 0.5) [
   %             draw,
   %             circle,
   %             minimum width = 1cm
   %         ] {b};
   %         \node (node-sc) at (1.5, -1) [
   %             draw,
   %             circle,
   %             minimum width = 1cm
   %         ] {c};
   %         
   %         \draw [->, dashed] (node-ca) -- (node-sa);
   %         \draw [->, dashed] (node-cb) -- (node-sb);
   %         \draw [->, dashed] (node-cc) -- (node-sc);
   %         
   %         \draw (node-sa) -- (node-sb) -- (node-sc);

   %         \node [
   %             above = 0.25cm of client
   %         ] {Client};
   %         \node [
   %             below = 0.25cm of server
   %         ] {Server};
   %     \end{tikzpicture}
   %     \caption{Illustration of the client-server communication in the 
   %              initial software implementation for inserting data, where $a$, $b$ and $c$ are nodes. Note that 
   %              the Client sends nodes \emph{one by one} to the Server, which 
   %              then constructs a treap using the node's compare method to 
   %              ensure the properties defined in \autoref{subsec:treaps}.}
   %     \label{fig:client-server}
   % \end{figure}

    \subsection{Unit Testing}
    \label{subsec:unit}

    In order to test whether the functionality of the implementations for the 
    treap, \gls{ore} and \gls{gc} are such as they have been defined in 
    \autoref{ch:background}, unit tests are implemented. These tests can be 
    executed separately from the main programming and test the behavior of 
    specific components of the software. Four unit test classes have been 
    implemented. All the unit tests are implemented in Python and are 
    included in \autoref{app:unit}. 

    Executing the unit test with the initial version of the code results in 
    four errors, all of which occur at tests that are part of \texttt{TestGC}. 
    The complete results of these tests are included in 
    \autoref{sec:test-results}. Upon further inspection of the code the cause 
    is evident, as the implementation for the \gls{gc} wrapper is incomplete. 
    Therefore, in order to be able to compare the \gls{ore} and \gls{gc} 
    methods, this particular part of the software must be implemented in a next 
    step.

