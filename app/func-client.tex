\chapter{Functional Block Diagrams}
\label{app:functional-block}

This appendix contains several function block diagrams representing components 
of the code base presented at the start of the project. These diagrams provide 
an overview of the flow of execution that can occur upon calls to certain 
functions within the code. Each block represents such a function and operators 
indicate whether a specific set of blocks is called ($\wedge$) or if one or none 
of a specific set of blocks is called ($\vee$). The order of execution of a set 
of block is strictly speaking not indicated by these diagrams, but might be 
hinted at by the order in which they are listed from top to bottom. In some 
cases relations, blocks and operators are drawn with dashed lines, indicating 
that some logical details have been omitted in favor of readability.

\tikzstyle {block} = [
    draw,
    font = \ttfamily
]

\tikzstyle {op} = [
    draw,
    circle
]

\begin{landscape}

    \section{\texttt{client.py}}    

    \begin{figure}[h]
        \centering
    \begin{tikzpicture}

        \node (client) [
            block
        ] {client.py};
        \node (and1) [
            op,
            right = 1cm of client
        ] {$\wedge$};
    
        \draw [->] (client) -- (and1);

        \node (insert-records) [
            block,
            above right = 1cm of and1
        ] {insert\_records()};
        \node (and2) [
            op,
            right = 2cm of insert-records
        ] {$\wedge$};
        \node (server-insert) [
            block,
            above right = 1cm of and2
        ] {server.insert()};

        \draw [->] (and1) |- (insert-records);
        \draw [->] (insert-records) -- (and2);
        \draw [->] (and2) |- (server-insert);

        \node (and3) [
            op,
            below right = 2cm and 1cm of and1
        ] {$\wedge$};
        \node (server-query) [
            block,
            below right = 1cm of and3
        ] {server.query()};
        \node (generate-node) [
            block,
            above right = 1cm of and3
        ] {generate\_node()};

        \draw [->] (and1) |- (and3);
        \draw [->] (and3) |- (server-query);
        \draw [->] (and3) |- (generate-node);

        \node (merge) [
            below = 1cm of and2
        ] {};
        \node (and4) [
            op,
            right = 1cm of merge.center
        ] {$\wedge$};
        \node (node) [
            block,
            above right = 1cm of and4
        ] {Node()};
        \node (or) [
            op,
            below right = 1cm of and4
        ] {$\vee$};
        \node (ore-encrypt) [
            block,
            above right = 1cm of or
        ] {ORE.encrypt()};
        \node (gc-generate) [
            block,
            below right = 1cm of or
        ] {GC.generate()};

        \draw [->] (and2) -- (merge.center);
        \draw [->] (generate-node) -| (merge.center);
        \draw [->] (merge.center) -- (and4);
        \draw [->] (and4) |- (node);
        \draw [->] (and4) |- (or);
        \draw [->] (or) |- (ore-encrypt);
        \draw [->] (or) |- (gc-generate);

        \node (oree-encrypt) [
            block,
            dashed,
            right = 1cm of ore-encrypt
        ] {LewiWuOREBlkLF.encrypt()};

        \node (arxgc-generate) [
            block,
            dashed,
            right = 1cm of gc-generate
        ] {arxgc.generate()};

        \draw [->, dashed] (ore-encrypt) -- (oree-encrypt);
        \draw [->, dashed] (gc-generate) -- (arxgc-generate);

    \end{tikzpicture}
    \end{figure}
\end{landscape}

\section{\texttt{server.py}: \texttt{Database}}

\begin{figure}[h]
    \begin{tikzpicture}
        \node (server) [
            block
        ] {Database};

        \node (or1) [
            op,
            right = 0.5cm of server
        ] {$\vee$};

        \draw [->] (server) -- (or1);

        \node (server-insert) [
            block,
            above right = 5cm and 0.5cm of or1.east
        ] {Database.insert()};
        \node (and1) [
            op,
            right = 0.5cm of server-insert
        ] {$\wedge$};
        \node (node1) [
            block,
            above right = 0.5cm of and1.east
        ] {Node()};
        \node (treap-insert) [
            block,
            below right = 0.5cm of and1.east
        ] {treap.insert()};

        \draw [->] (or1) |- (server-insert);
        \draw [->] (server-insert) -- (and1);
        \draw [->] (and1) |- (node1);
        \draw [->] (and1) |- (treap-insert);

        \node (server-count) [
            block,
            above right = 3cm and 0.5cm of or1.east
        ] {Database.count()};
        \node (treap-len) [
            block,
            right = 0.5cm of server-count
        ] {treap.\_\_len\_\_()};

        \draw [->] (or1) |- (server-count);
        \draw [->] (server-count) -- (treap-len);

        \node (server-smallest) [
            block,
            above right = 1cm and 0.5cm of or1.east
        ] {Database.smallest()};
        \node (and2) [
            op,
            right = 0.5cm of server-smallest
        ] {$\wedge$};
        \node (treap-find-min) [
            block,
            above right = 0.5cm of and2.east
        ] {treap.find\_min()};
        \node (treap-find1) [
            block,
            below right = 0.5cm of and2.east
        ] {treap.find()};

        \draw [->] (or1) |- (server-smallest);
        \draw [->] (server-smallest) -- (and2);
        \draw [->] (and2) |- (treap-find-min);
        \draw [->] (and2) |- (treap-find1);

        \node (server-biggest) [
            block,
            below right = 1cm and 0.5cm of or1.east
        ] {Database.biggest()};
        \node (and3) [
            op,
            right = 0.5cm of server-biggest
        ] {$\wedge$};
        \node (treap-find-max) [
            block,
            above right = 0.5cm of and3.east
        ] {treap.find\_max()};
        \node (treap-find2) [
            block,
            below right = 0.5cm of and3.east
        ] {treap.find()};

        \draw [->] (or1) |- (server-biggest);
        \draw [->] (server-biggest) -- (and3);
        \draw [->] (and3) |- (treap-find-max);
        \draw [->] (and3) |- (treap-find2);

        \node (server-find) [
            block,
            below right = 3.5cm and 0.5cm of or1.east
        ] {Database.find()};
        \node (and4) [
            op,
            right = 0.5cm of server-find
        ] {$\wedge$};
        \node (node2) [
            block,
            above right = 0.5cm of and4.east
        ] {Node()};
        \node (treap-find3) [
            block,
            below right = 0.5cm of and4.east
        ] {treap.find()};

        \draw [->] (or1) |- (server-find);
        \draw [->] (server-find) -- (and4);
        \draw [->] (and4) |- (node2);
        \draw [->] (and4) |- (treap-find3);

        \node (server-query) [
            block,
            below right = 6.5cm and 0.5cm of or1.east
        ] {Database.query()};
        \node (and5) [
            op,
            right = 0.5cm of server-query
        ] {$\wedge$};
        \node (node3) [
            block,
            above right = 1cm and 0.5cm of and5.east
        ] {Node()};
        \node (treap-find-closest) [
            block,
            right = 0.5cm of and5.east
        ] {treap.root.find\_node\_clostest()};
        \node (treap-successor) [
            block,
            below right = 1cm and 0.5cm of and5.east
        ] {treap.successor()};

        \draw [->] (or1) |- (server-query);
        \draw [->] (server-query) -- (and5);
        \draw [->] (and5) |- (node3);
        \draw [->] (and5) -- (treap-find-closest);
        \draw [->] (and5) |- (treap-successor);
    \end{tikzpicture}
\end{figure}

\newpage
\section{\texttt{treap.py}: \texttt{treap}}

\begin{figure}[h]
    \begin{tikzpicture}
        \node (treap) [
            block
        ] {treap};
        \node (or1) [
            op,
            right = 0.5cm of treap
        ] {$\vee$};

        \draw [->] (treap) -- (or1);

        \node (treap-insert) [
            block,
            above right = 6cm and 0.5cm of or1.east
        ] {treap.insert()};
        \node (and1) [
            op,
            right = 0.5cm of treap-insert
        ] {$\wedge$};
        \node (treap-node) [
            block,
            above right = 0.5cm of and1.east
        ] {treap\_node()};
        \node (root-insert) [
            block,
            below right = 0.5cm of and1.east
        ] {root.insert()};

        \draw [->] (or1) |- (treap-insert);
        \draw [->] (treap-insert) -- (and1);
        \draw [->] (and1) |- (treap-node);
        \draw [->] (and1) |- (root-insert);

        \node (len) [
            block,
            above right = 4cm and 0.5cm of or1.east
        ] {\_\_len\_\_()};

        \draw [->] (or1) |- (len);

        \node (find) [
            block,
            right = 0.5cm of or1.east
        ] {treap.find()};
        \node (merge) [
            right = 5cm of or1.east
        ] {};
        \node (node-compare) [
            block,
            dashed,
            right = 0.5cm of merge.center
        ] {Node.compare()};

        \draw [->] (or1) -- (find);
        \draw (find) -- (merge.center);

        \node (successor) [
            block,
            above right = 2cm and 0.5cm of or1.east
        ] {treap.successor()};
        \node (and2) [
            op,
            above = 2cm of merge.center
        ] {$\wedge$};
        \node (treap-node-find-min) [
            block,
            right = 0.5cm of and2.east
        ] {treap\_node.find\_min\_node()};

        \draw [->] (or1) |- (successor);
        \draw [->] (successor) -- (and2);
        \draw [->] (and2) -- (treap-node-find-min);
        \draw [->] (and2) -- (merge.center);

        \node (predecessor) [
            block,
            below right = 2cm and 0.5cm of or1.east
        ] {treap.predecessor()};
        \node (and3) [
            op,
            below = 2cm of merge.north
        ] {$\wedge$};
        \node (treap-node-find-max) [
            block,
            right = 0.5cm of and3.east
        ] {treap\_node.find\_max\_node()};

        \draw [->] (or1) |- (predecessor);
        \draw [->] (predecessor) -- (and3);
        \draw [->] (and3) -- (treap-node-find-max);
        \draw [->] (and3) -- (merge.center);

        \draw [->, dashed] (merge.center) -- (node-compare);

        \node (find-max) [
            block,
            below right = 4cm and 0.5cm of or1.east
        ] {treap.find\_max()};

        \draw [->] (or1) |- (find-max);

        \node (find-min) [
            block,
            below right = 6cm and 0.5cm of or1.east
        ] {treap.find\_min()};

        \draw [->] (or1) |- (find-min);
    \end{tikzpicture}
\end{figure}

\newpage
\section{\texttt{treap.py}: \texttt{treap\_node}}

\begin{figure}[h]
    \begin{tikzpicture}
        \node (treap-node1) [
            block
        ] {treap\_node};
        \node (or1) [
            op,
            right = 0.5cm of treap-node1
        ] {$\vee$};

        \draw [->] (treap-node1) -- (or1);

        \node (insert1) [
            block,
            below right = 5cm and 0.5cm of or1.east
        ] {treap\_node.insert()};
        \node (or2) [
            op,
            right = 0.5cm of insert1
        ] {$\vee$};
        \node (treap-node2) [
            block,
            above right = 0.5cm of or2.east
        ] {treap\_node()};
        \node (and1) [
            op,
            below right = 2cm and 0.5cm of or2.east
        ] {$\wedge$};
        \node (or3) [
            op,
            above right = 1cm and 0.5cm of and1.east
        ] {$\vee$};
        \node (rotate-left1) [
            block,
            above right = 0.5cm of or3.east
        ] {treap\_node.rotate\_with\_left()};
        \node (rotate-right1) [
            block,
            below right = 0.5cm of or3.east
        ] {treap\_node.rotate\_with\_right()};
        \node (insert2) [
            block,
            below right = 0.5cm of and1.east
        ] {treap\_node.insert()};

        \draw [->] (or1) |- (insert1);
        \draw [->] (insert1) -- (or2);
        \draw [->] (or2) |- (treap-node2);
        \draw [->] (or2) |- (and1);
        \draw [->] (and1) |- (or3);
        \draw [->] (or3) |- (rotate-left1);
        \draw [->] (or3) |- (rotate-right1);
        \draw [->] (and1) |- (insert2);

        \node (min-node) [
            block,
            above right = 3cm and 0.5cm of or1.east
        ] {treap\_node.find\_min\_node()};

        \draw [->] (or1) |- (min-node);

        \node (max-node) [
            block,
            above right = 1cm and 0.5cm of or1.east
        ] {treap\_node.find\_max\_node()};

        \draw [->] (or1) |- (max-node);

        \node (rotate-left1) [
            block,
            below right = 1cm and 0.5cm of or1.east
        ] {treap\_node.rotate\_with\_left()};

        \draw [->] (or1) |- (rotate-left1);

        \node (rotate-right1) [
            block,
            below right = 3cm and 0.5cm of or1.east
        ] {treap\_node.rotate\_with\_right()};

        \draw [->] (or1) |- (rotate-right1);

        \node (node-closest) [
            block,
            above right = 5cm and 0.5cm of or1.east
        ] {treap\_node.find\_node\_clostest()};
        \node (node-compare) [
            block,
            dashed,
            right = 0.5cm of node-closest
        ] {Node.compare()};

        \draw [->] (or1) |- (node-closest);
        \draw [->, dashed] (node-closest) -- (node-compare);
    \end{tikzpicture}
\end{figure}

\newpage
\section{\texttt{node.py}: \texttt{Node}}

\begin{figure}[h]
    \begin{tikzpicture}
        \node (node-compare) [
            block
        ] {Node.compare()};
        \node (or1) [
            op,
            right = 0.5cm of node-compare
        ] {$\vee$};

        \draw [->] (node-compare) -- (or1);

        \node (ore-compare) [
            block,
            above right = 1cm and 0.5cm of or1.east
        ] {ORE.compare()};
        \node (oree-compare) [
            block,
            dashed,
            right = 0.5cm of ore-compare
        ] {LewiWuOREBlkLF.compare()};

        \draw [->] (or1) |- (ore-compare);
        \draw [->, dashed] (ore-compare) -- (oree-compare);

        \node (gc-evaluate) [
            block,
            right = 0.5cm of or1.east
        ] {GC.evaluate()};
        \node (arxgc-evaluate) [
            block,
            dashed,
            right = 0.5cm of gc-evaluate
        ] {arxgc.evaluate()};
        
        \draw [->] (or1) -- (gc-evaluate);
        \draw [->, dashed] (gc-evaluate) -- (arxgc-evaluate);

        \node (plain-compare) [
            block,
            below right = 1cm and 0.5cm of or1.east
        ] {PLAIN.compare()};

        \draw [->] (or1) |- (plain-compare);
    \end{tikzpicture}
\end{figure}

