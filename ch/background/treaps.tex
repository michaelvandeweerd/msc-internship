\section{Treaps and Related Data Structures}
\label{sec:treaps}

Whereas the \emph{binary search tree} and \emph{binary heap} data structures 
provide distinct characteristics, \emph{treaps} combine the capabilities of 
both, hence its name being a portmanteau of ``tree'' and ``heap''. In this 
section, the specifics of both binary search trees and binary heaps will be 
recapped, allowing for a smooth transition to the definition of treaps. In the 
literature, one may find a concept closely related to treaps called 
\emph{randomized binary search trees}, which will not be covered in this 
report. Both the binary search tree and the binary heap are extensions upon the 
concept of a binary tree. For completeness sake, the definition of binary trees 
is also covered in this section.

    \subsection{Binary Trees}
    \label{subsec:binary-trees}

    \cite{liang:2013} shows us that the binary tree is a data structure, 
    consisting of \emph{nodes}, each of which optionally has a \emph{left 
    child} or \emph{right child}, which are nodes as well. A node that is not 
    a child of any other node in the tree is called the \emph{root}, while a 
    node having neither a left nor right child is called a \emph{leaf}.
    Nodes that are a child of the same node --- i.e. the \emph{parent} --- are 
    called \emph{siblings}. The length of the path between a node and the root 
    is referred to as \emph{depth}, allowing for sets of nodes having the same 
    depth to be defined as a \emph{level}. We can construct sub-trees by 
    considering a node in the tree to be a root. Several of these concepts 
    have been illustrated in \autoref{fig:binary-trees}.

    \tikzstyle{node} = [
        draw,
        circle,
        minimum width = 1cm,
        minimum height = 1cm
    ]

    \tikzstyle{path} = [
        bend right,
        ->,
        dashed
    ]

    \begin{figure}[h!]
        \centering
        \begin{tikzpicture}
            \node (v) [
                node
            ] {$v$};

            \node (a) [
                node,
                below left = 1cm of v,
                label = left:{Left child}
            ] {};

            \node (b) [
                node,
                below right = 1cm of v,
                label = right:{Right child}
            ] {};

            \node (r) [
                node,
                above right = 1cm of v,
                label = right:{Root}
            ] {};

            \node (d) [
                node,
                below right = 1cm of r,
                label = right:{Sibling}
            ] {};

            \draw (v) -- (a);
            \draw (v) -- (b);
            \draw (r) -- (v);
            \draw (r) -- (d);
        \end{tikzpicture}

        \caption{An illustration of several definition related to binary trees. 
            Each label refers to the relation of the node in question to node 
            $v$.}
        \label{fig:binary-trees}
    \end{figure}

    \subsection{Binary Search Trees}
    \label{subsec:binary-search-trees}

    \newacronym{bst}{BST}{Binary Search Tree}

    According to \cite{liang:2013}, what separates the \gls{bst} from an 
    ``ordinary'' binary tree is that, for every node $v$ in a binary tree, 
    having a left child and a right child being the roots of sub-trees $A$ and 
    $B$ respectively:

        $$\forall a \in A, \forall b \in B : a < v < b.$$

    Intuitively, this means that every node in a \gls{bst} will have nodes of 
    a \emph{lower} value on its left and nodes of a \emph{higher} value on its 
    right when traversing down the tree structure. As a result, visiting the 
    nodes in a \gls{bst} using \emph{in-order traversal} will result in a list 
    of nodes ordered by increasing value. This concept is illustrated in 
    \autoref{fig:binary-search-tree}.

    \begin{figure}[h!]
        \centering
        \begin{tikzpicture}
            \node (a) [
                node
            ] {20};

            \node (b) [
                node,
                below left = 1cm of a
            ] {19};

            \node (c) [
                node,
                below right = 1cm of a
            ] {24};

            \node (d) [
                node,
                below left = 1cm of c
            ] {21};

            \draw (a) -- (b);
            \draw (a) -- (c);
            \draw (c) -- (d);

            \draw [
                path
            ] (a) to node [auto, swap] {1} (b);

            \draw [
                path
            ] (b) to node [auto, swap] {2} (a);

            \draw [
                path
            ] (a) to node [auto, swap] {3} (c);

            \draw [
                path
            ] (c) to node [auto, swap] {4} (d);

            \draw [
                path
            ] (d) to node [auto, swap] {5} (c);
	\end{tikzpicture}

        \caption{An example of a \gls{bst}. Note that for each node, the left 
            child subtree only contains nodes with a lower value, while the 
            right child subtree only contains nodes with a higher value. The 
            dashed arrows indicate the path that emerges when visiting each 
            node using in-order traversal, resulting in the ordered listing 
            of the node values: 19, 20, 21, 24.}
        \label{fig:binary-search-tree}
    \end{figure}

    \subsection{Binary Heaps}
    \label{subsec:binary-heaps}

    A binary heap is a binary tree that meets the \emph{heap property}, as 
    explained by \cite{liang:2013}. This property states that for each node 
    $v$ in a heap, having a left child and a right child $a$ and $b$ 
    respectively:

        $$\max(a, b) \leq v,$$

    \noindent or, depending on the application:

        $$\min(a, b) \geq v.$$

    Intuitively this means that every node has a value greater or equal to its 
    left child and right child. The nodes of a binary heap can be stored in an 
    array, where the left and right child of a node at position $i$ can be 
    found at position $2i + 1$ and position $2i + 2$ respectively. An 
    illustration of this concept has been included in 
    \autoref{fig:binary-heaps}.

    \begin{figure}[h!]
        \centering
        \begin{tikzpicture}
            \node (a) [
                node
            ] {42};

            \node (b) [
                node,
                below left = 1cm of a
            ] {32};

            \node (c) [
                node,
                below left = 1cm of b
            ] {22};

            \node (d) [
                node,
                below right = 1cm of b
            ] {29};

            \node (e) [
                node,
                below right = 1cm of a
            ] {39};

            \draw (a) -- (b);
            \draw (b) -- (c);
            \draw (b) -- (d);
            \draw (a) -- (e);
        \end{tikzpicture}

        \caption{An example of a binary heap. Note that the left child and 
            right child of each node has a smaller value than the node itself,
            i.e. $\max(a, b) \leq v$. The nodes in this heap can be stored as 
            the array $[42, 32, 39, 22, 29]$.}
        \label{fig:binary-heaps}
    \end{figure}

    \subsection{Treaps}
    \label{subsec:treaps}

    As mentioned before, treaps combine the features of \glspl{bst} and binary 
    heaps. Concretely, this means that a treap has the property of order 
    identical to that of the \gls{bst} and the heap property. This is achieved 
    by assigning \emph{two} values to each node, a \emph{key} and a 
    \emph{priority}. In the context of treaps, we can define a node as 
    a tuple containing a key and priority a follows: $(k, p)$. Given a treap 
    having a node $(v_k, v_p)$ having a left child $(\alpha_k, \alpha_p)$ and 
    a right child $(\beta_k, \beta_p)$, both of which are roots of the 
    sub-trees $A$ and $B$ respectively, then the following proposition must be 
    true:

        $$(\forall (a_k, a_p) \in A, \forall (b_k, b_p) \in B : a_k < v_k < b_k) \wedge (\max(\alpha_p, \beta_p) \leq v_p).$$

    Intuitively this means that a tree constructed from the keys of each node 
    in a treap must be a valid \gls{bst} and a tree constructed from the 
    priorities of each node in a treap must be a valid binary heap. An example 
    of a treap is provided in \autoref{fig:treaps}.

    \begin{figure}[h!]
        \centering
        \begin{tikzpicture}

            %     d           6           2
            %    / \         / \         / \
            %   b   e       2   8       3   3
            %  / \   \     / \   \     / \   \
            % a   c   f   1   5   9   8   3   5

            \node (a) [
                node
            ] {1, 8};

            \node (b) [
                node,
                above right = 1cm of a
            ] {2, 3};

            \node (c) [
                node,
                below right = 1cm of b
            ] {5, 3};

            \node (d) [
                node, 
                above right = 1cm of b
            ] {6, 2};

            \node (e) [
                node,
                below right = 1cm of d
            ] {8, 3};

            \node (f) [
                node,
                below right = 1cm of e
            ] {9, 5};

            \draw (a) -- (b);
            \draw (b) -- (c);
            \draw (b) -- (d);
            \draw (d) -- (e);
            \draw (e) -- (f);
        \end{tikzpicture}

        \caption{An example of a treap. Note that the key --- the left part 
            of each label --- maintains the ordering property of a \gls{bst} 
            while the priority --- the right part of each label --- preserves 
            the heap property, which is defined as $\min(a, b) \geq v$.}
        \label{fig:treaps}
    \end{figure}

