\section{Cryptographic Techniques}
\label{sec:crypt}

This section describes the workings of the cryptographic techniques that are 
considered in this project. In general, these techniques are used to convert 
\emph{plaintext} into \emph{ciphertext} while still allowing to perform 
certain computations over the latter as if it where the prior. The goal 
of cryptography is to reduce the possibilities of reversing this process 
without the required prior knowledge, i.e. a key.

    \subsection{Order-Revealing Encryption}
    \label{subsec:ore}

    \newacronym{ppe}{PPE}{Property-Preserving Encryption}
    \newacronym{ope}{OPE}{Order-Preserving Encryption}
    \newacronym{cpa}{CPA}{Chosen-Plaintext Attack}

    In order to gain a solid understanding of the concept of \gls{ore}, it is 
    useful to be familiar with the related concept of \gls{ope}. 
    \cite{lewi+wu:2016} describe an \gls{ope} as an encryption scheme that 
    allows for the comparison of encrypted values. Being able to compare 
    values means knowing the order of values. This is achieved by having the 
    comparison operation result in an indication of the difference of one 
    value to another, e.g. -1, 0 or 1 if value $a$ is respectively smaller 
    than, equal to or greater than value $b$. \cite{lewi+wu:2016} explain that 
    \gls{ope} is a so-called \gls{ppe}, an encryption scheme that allows for 
    ciphertexts to reveal a specific property of the underlying plaintext. 
    Unfortunately, research referenced by \cite{lewi+wu:2016} has shown that 
    this mechanism within \gls{ope} also results in a significant amount of 
    information leaks about the encrypted plaintexts.

    The issue of information leakage in \gls{ope} is resolved in the 
    definition of \gls{ore}, provided in \cite{lewi+wu:2016}. They state that, 
    in contrast to \gls{ope}, \gls{ore} does not impose any constraints on 
    ciphertexts --- e.g. ordered numeric values --- but requires that a 
    comparison function is provided that is able to compute comparisons 
    between ciphertexts. The \gls{ore} scheme consists of three algorithms: 
    \emph{ORE.Setup}, \emph{ORE.Encrypt} and \emph{ORE.Compare}. The 
    (simplified) properties of the algorithms as described in 
    \cite{lewi+wu:2016} and \cite{chenette+lewi+weis+wu:2015} are: 

    \begin{description}
        \item[$\bm{\text{ORE.Setup}() \to k}$]
            The algorithms returns a secret key $k$.
        \item[$\bm{\text{ORE.Encrypt}(k, m) \to c}$]
            Given a secret key $k$ and a plaintext input message $m$, the 
            algorithms returns a ciphertext $c$.
        \item[$\bm{\text{ORE.Compare}(c_1, c_2) \to b}$]
            Given two ciphertexts $c_1$ and $c_2$, the algorithm returns a 
            value $b \in \{0, 1\}$.
    \end{description}

    Note that the scheme does not include a decryption algorithm. 
    \cite{chenette+lewi+weis+wu:2015} explain that this is due to the 
    generic nature of the \gls{ore} scheme. They argue that this 
    functionality can be implemented using the available algorithms or 
    by extending the encryption algorithm to be \gls{cpa} secure, meaning a 
    symmetric encryption key is required.

    \subsection{Garbled Circuits}
    \label{subsec:gc}

    \cite{boelter+poddar+popa:2017} describe \glspl{gc} as a cryptographic 
    technique that ``encrypts'' logical circuits such that the functionality 
    is preserved. Due to the encrypts, the internal logic of the circuit 
    cannot be evaluated in a meaningful way, allowing for critical 
    information to be obscured. Furthermore, \cite{boelter+poddar+popa:2017} 
    provide an overview of the \gls{gc} scheme, which consist of four 
    algorithms: \emph{GC.Garble}, \emph{GC.Encode}, \emph{GC.Eval} and 
    \emph{GC.Decode}. A graphical representation of the data flow when using 
    a \gls{gc} has been included in \autoref{fig:gc}. Next, the definitions 
    of these algorithms will be presented as they have been given by 
    \cite{boelter+poddar+popa:2017}:

    \begin{description}
        \item[$\bm{\text{GC.Garble}(f) \to (F, e, d)}$]
            Given a binary circuit $f$, the algorithm returns a garbled 
            circuit $F$, encoding information $e$ and decoding information 
            $d$.
        \item[$\bm{\text{GC.Encode}(e, x) \to X}$]
            Given encoding information $e$ and plain input $x$, the algorithms 
            returns a garbled input $X$, provided that $x$ is a valid input 
            for $f$.
        \item[$\bm{\text{GC.Eval}(F, X) \to Y}$]
            Given a garbled circuit $F$ and a garbled input $X$, the algorithm 
            returns a garbled output $Y$.
        \item[$\bm{\text{GC.Decode}(d, Y) \to y}$]
            Given decoding information $d$ and garbled output $Y$, the 
            algorithm returns plain output $y$.
    \end{description}

    \subsection{Branch-Chained Garbled Circuits}
    \label{subsec:bcgc}

    In their master's thesis, \cite{boelter+poddar+popa:2017} introduce the 
    notion of \emph{branch-chained garbled circuits}. These constructions are 
    treaps where each node is a \gls{gc}. When traversing the tree, 
    starting at the root, the output of the \gls{gc} at each node determines 
    which of its two children will be the next node to visit. Considering the 
    branch-chained \gls{gc} as a scheme of its own, 
    \cite{boelter+poddar+popa:2017} define it to consist of three algorithms:
    \emph{BCGC.Generate}, \emph{BCGC.Encode} and \emph{BCGC.Eval}. In 
    \autoref{fig:bcgc} a graphical representation is included of the data 
    streams when using branch-chained \glspl{gc}. \cite{
    boelter+poddar+popa:2017} provide algorithms for the aforementioned methods, 
    which are included in this report in a simplified form:

    \begin{description}
        \item[$\bm{\text{BCGC.Generate}(f, e_1, e_2) \to (F, e)}$]
            Given a boolean circuit $f$ and encoding information $e_1$ and 
            $e_2$, the algorithm returns a branch-chained garbled circuit $F$ 
            and encoding information $e$.
        \item[$\bm{\text{BCGC.Encode}(e, x) \to X}$]
            Identical to the encode algorithm for \gls{gc} schemes as 
            presented in \autoref{subsec:gc}.
        \item[$\bm{\text{BCGC.Eval}(F, X_1) \to (b, X_2)}$]
            Given a branch-chained garbled circuit $F$ and a garbled input 
            $X_1$, the algorithm returns a bit $b$ indicating whether the next 
            node will be the left or right child of $F$ and a garbled input 
            $X_2$ to be used as input for the evaluation of the next garbled 
            circuit.
    \end{description}

    \begin{figure}[h]
        \centering
        \begin{tikzpicture}
            
            \tikzstyle{var} = [
                minimum height = 0.5cm 
            ];

            \tikzstyle{par} = [
                minimum height = 0.75cm
            ];

            \tikzstyle{box} = [
                draw,
                par
            ];

            \node (A) at (-2, 0) {A};
            \node (B) at (2, 0) {B};

            \node (f) at (-3, -1) [
                var
            ] {$f$};
            \node (x) at (-1, -1) [
                var
            ] {$x$};

            \node (garble-a) at (-4, -2) [
                par
            ] {};
            \node (garble-b) at (-3, -2) [
                par
            ] {};
            \node (garble-c) at (-2, -2) [
                par
            ] {};
            \node (garble) at (-3, -2) [
                box,
                minimum width = 3cm
            ] {\texttt{garble}};

            \node (d) at (-4, -3) [
                var
            ] {$\strut d$};
            \node (F) at (-3, -3) [
                var
            ] {$\strut F$};
            \node (e) at (-2, -3) [
                var
            ] {$\strut e$};

            \node (encode-a) at (-2, -4) [
                par
            ] {};
            \node (encode-b) at (-1, -4) [
                par
            ] {};
            \node (encode) at (-1.5, -4) [
                box,
                minimum width = 2cm
            ] {\texttt{encode}};

            \node (X) at (-1.5, -5) [
                var
            ] {$X$};

            \node (Fb) at (0, -6.25) [
                par
            ] {};
            \node (Xb) at (0, -5.75) [
                par
            ] {};

            \node (eval-a) at (1.5, -7) [
                par
            ] {};
            \node (eval-b) at (2.5, -7) [
                par
            ] {};
            \node (eval) at (2, -7) [
                box,
                minimum width = 2cm
            ] {\texttt{eval}};

            \node (Y) at (2, -8) [
                var
            ] {$\strut Y$};

            \node (Yb) at (0, -9) [
                par
            ] {};

            \node (decode-a) at (-4, -10) [
                par
            ] {};
            \node (decode-b) at (-3, -10) [
                par
            ] {};
            \node (decode) at (-3.5, -10) [
                box,
                minimum width = 2cm
            ] {\texttt{decode}};

            \node (y) at (-3.5, -11)  [
                var
            ] {$\strut y$};

            \draw [->] (f) -- (garble-b);
            \draw [->] (garble-a) -- (d);
            \draw [->] (garble-b) -- (F);
            \draw [->] (garble-c) -- (e);
            \draw [->] (e) -- (encode-a);
            \draw [->] (x) -- (encode-b);
            \draw [->] (encode) -- (X);
            \draw (F) |- (Fb.center);
            \draw (X) |- (Xb.center);
            \draw [->] (Fb.center) -| (eval-a);
            \draw [->] (Xb.center) -| (eval-b);
            \draw [->] (eval) -- (Y);
            \draw (Y) |- (Yb.center);
            \draw [->] (d) -- (decode-a);
            \draw [->] (Yb.center) -| (decode-b);
            \draw [->] (decode) -- (y);
            \draw [dashed] (0, 0.5) -- (0, -11.5);
        \end{tikzpicture}

        \caption{Graphical illustration of the data flow when using a 
                 \gls{gc}, demonstrating how client B can be utilized to 
                 evaluate binary circuit $f$ with input $x$ without ever 
                 knowing about what these objects are due to the garbling 
                 and encoding applied by client A.}
        \label{fig:gc}
    \end{figure}

    \begin{figure}[h]
        \centering
        \begin{tikzpicture}

            \tikzstyle{arg} = [
                minimum height = 0.75cm
            ];

            \tikzstyle{box} = [
                arg,
                draw
            ];

            \node (A) at (-2, 0) {A};
            \node (B) at (2, 0) {B};

            \node (d) at (-3, -1) {$\strut d$};
            \node (e) at (-2, -1) {$\strut e$};
            \node (x) at (-1, -1) {$\strut x$};

            \node (F0) at (2, -1) {$\strut F_0$};
            \node (F1) at (3, -1) {$\strut F_1$};
            \node (F2) at (4, -1) {$\strut F_2$};

            \node (encode-a) at (-2, -2) [arg] {};
            \node (encode-b) at (-1, -2) [arg] {};            

            \node (encode) at (-1.5, -2) [
                box,
                minimum width = 2cm
            ] {\texttt{encode}};

            \node (X0) at (-1.5, -3) {$\strut X_0$};
            \node (X0b) at (0, -4) {};

            \node (eval0-a) at (1, -5) [arg] {};
            \node (eval0-b) at (2, -5) [arg] {};

            \node (eval0) at (1.5, -5) [
                box,
                minimum width = 2cm
            ] {\texttt{eval}};

            \node (X1) at (2, -6) {$\strut X_1$};
            
            \node (eval1-a) at (2, -7) [arg] {};
            \node (eval1-b) at (3, -7) [arg] {};

            \node (eval1) at (2.5, -7) [
                box,
                minimum width = 2cm
            ] {\texttt{eval}};

            \node (X2) at (3, -8) {$\strut X_2$};

            \node (eval2-a) at (3, -9) [arg] {};
            \node (eval2-b) at (4, -9) [arg] {};

            \node (eval2) at (3.5, -9) [
                box,
                minimum width = 2cm
            ] {\texttt{eval}};

            \node (Y) at (3.5, -10) {$\strut Y$};
            \node (Yb) at (0, -11) {};

            \node (decode-a) at (-3, -12) [arg] {};
            \node (decode-b) at (-2, -12) [arg] {};

            \node (decode) at (-2.5, -12) [
                box,
                minimum width = 2cm
            ] {\texttt{decode}};

            \node (y) at (-2.5, -13) {$\strut y$};

            \draw [->] (d) -- (decode-a);
            \draw [->] (e) -- (encode-a);
            \draw [->] (x) -- (encode-b);
            \draw [->] (encode) -- (X0);
            \draw (X0) |- (X0b.center);
            \draw [->] (X0b.center) -| (eval0-a);
            \draw [->] (F0) -- (eval0-b);
            \draw [->] (eval0-b) -- (X1);
            \draw [->] (X1) -- (eval1-a);
            \draw [->] (F1) -- (eval1-b);
            \draw [->] (eval1-b) -- (X2);
            \draw [->] (X2) -- (eval2-a);
            \draw [->] (F2) -- (eval2-b);
            \draw [->] (eval2) -- (Y);
            \draw (Y) |- (Yb.center);
            \draw [->] (Yb.center) -| (decode-b);
            \draw [->] (decode) -- (y);

            \draw [dashed] (0, 0.5) -- (0, -13.5);
        \end{tikzpicture}

        \caption{Graphical representation of the data flow when using 
                 branch-chained \gls{gc}. Client A has provided client B 
                 with a collection of \glspl{gc} earlier, generated as 
                 described in \autoref{subsec:bcgc}. The output of each 
                 evaluation can be used to determine which \gls{gc} to 
                 use next. In the context of this report, client B stores 
                 the \glspl{gc} in a \emph{treap} data structure.}
        \label{fig:bcgc}
    \end{figure}

